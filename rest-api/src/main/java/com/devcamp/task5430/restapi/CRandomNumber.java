package com.devcamp.task5430.restapi;

import java.util.Random;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CRandomNumber {
    @GetMapping("/devcamp5430/random-double") //Viết 1 ứng dụng java tạo số ngẫu nhiên kiểu double từ 1-100
    public String randomDoubleNumber(){
        Random random = new Random(); 
        double randomDoubleNumber = 1 + random.nextDouble() * 99.0;        
        return "Random value in double from 1 to 100: " + randomDoubleNumber;
    }

    @GetMapping("/devcamp5430/random-int") //Viết 1 ứng dụng java tạo số ngẫu nhiên kiểu int từ 1-10
    public String randomIntNumber(){
        Random random = new Random();
        int randomIntNumber = 1 + random.nextInt(10 - 1 + 1);       
       return "Random value in int from 1 to 10: " + randomIntNumber;
    }
}
