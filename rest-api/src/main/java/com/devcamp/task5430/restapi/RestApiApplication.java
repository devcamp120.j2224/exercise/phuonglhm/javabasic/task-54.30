package com.devcamp.task5430.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
		//khai báo đối tượng class CRandomNumber()
		CRandomNumber randomNumber = new CRandomNumber();
		//in số ngẫu nhiên kiểu double 1-100 ra console
		System.out.println(randomNumber.randomDoubleNumber());
		//in số ngẫu nhiên kiểu int 1-10 ra console
		System.out.println(randomNumber.randomIntNumber());

	}

}
